<?php

class DefaultController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
				// captcha action renders the CAPTCHA image displayed on the contact page
				'captcha'=>array(
						'class'=>'CCaptchaAction',
						'backColor'=>0xFFFFFF,
				),

		);
	}
		
	public function actionIndex()
	{
		$model=new UpgradeForm;
		if(isset($_POST['UpgradeForm']))
		{
			
			$model->attributes=$_POST['UpgradeForm'];
			if($model->validate())
			{
				if (!Yii::app()->user->isGuest){
					exec(Yii::app()->getModule('upgrade')->scriptPath, $output, $return);
				
					$this->render('result',array('output'=>$output, 'return'=>$return));
				} else {
					$this->render('index');
				}
			}
		} else {
			$this->render('upgrade',array('model'=>$model));
		}
			
	}
}