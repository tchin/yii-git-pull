This project is for update website using git as alternative to 
ftp.

Installation guide:
Implementation of pull script
1. copy to the pull.sh script to /var/script folder
2. chown www-data:www-data /var/script
3. chmod 755 /var/script/pull.sh
Implementation of upgrade module in yii website
1. copy upgrade folder to your protected/modules
2. cofigure protected/config/main to include below code in modules:
	'modules'=>array(
		// uncomment the following to enable the Gii tool
		
		'upgrade'=>array(
			'scriptPath' => '/var/script/pull.sh'
		)
	),

Implemenation on Server
1. make sure you PHP server support exec command
2. install git on linux that running PHP server
3. make sure your website folder and files ownership is www-data

configure git repository
1. Got to git config in the repository located at .git/config
2. add below line
[credential]
	helper = store --file=/var/script/gitcredential

3. create gitcredential in /var/script and add a below line in file
https://[username]:[password]@bitbucket.org


